from pathlib import Path
import pickle
import logging
import time

import numpy as np
from mpi4py import MPI
from .utilities import MyBaseModel


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()



def affine_trans(A, X):
    """
    Performs the 2D affine transformation, A (3x3), on each 2D coordinate
    located in the array, X (2xN).
    """
    shape = X.shape
    assert shape[0] == 2
    X = np.vstack([X, np.ones((shape[1]))])

    Y = A @ X

    return Y[:-1, :]



class LoggerBase(MyBaseModel):
    pass


class DataDumper(LoggerBase):
    plot_xlim: list
    plot_ylim: list
    plot_padding: float
    R: float
    zhub: float
    Uamb: float
    dumpdir: Path = Path("dump")
    Ainv: list
    log_interval: int
    resolution: int = 150

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.dumpdir.mkdir(parents=True, exist_ok=True)
        self.Ainv = np.array(self.Ainv)
        self.A = np.linalg.inv(self.Ainv)

    def log(self, hawc2farm):
        if hawc2farm.istep % self.log_interval != 0:
            return
        fn_out = f"datadump_{hawc2farm.istep:07}.pkl"

        # get current location of turbines
        x = hawc2farm.turbine.x_pos
        xs = comm.allgather(x)

        y = hawc2farm.turbine.y_pos
        ys = comm.allgather(y)

        z = hawc2farm.turbine.z_pos
        zs = comm.allgather(z)

        if rank == 0:
            xmin, xmax = (
                self.plot_xlim[0] - self.plot_padding,
                self.plot_xlim[1] + self.plot_padding,
            )
            ymin, ymax = (
                self.plot_ylim[0] - self.plot_padding,
                self.plot_ylim[1] + self.plot_padding,
            )

            Xs = np.linspace(xmin, xmax, self.resolution)
            Ys = np.linspace(ymin, ymax, self.resolution)
            Zs = [hawc2farm.zhub]

            Xmesh_g, Ymesh_g, Zmesh_g = np.meshgrid(Xs, Ys, Zs, indexing="ij")

            blah = affine_trans(
                self.A, np.vstack([Xmesh_g.ravel(), Ymesh_g.ravel()])
            )
            Xmesh_m = blah[0, :].reshape(Xmesh_g.shape)
            Ymesh_m = blah[1, :].reshape(Ymesh_g.shape)
            Zmesh_m = Zmesh_g

        else:
            Xmesh_m = np.empty((self.resolution, self.resolution, 1), dtype=np.float64)
            Ymesh_m = np.empty((self.resolution, self.resolution, 1), dtype=np.float64)
            Zmesh_m = np.empty((self.resolution, self.resolution, 1), dtype=np.float64)

        # MPI routines - MUST BE CALLED BY ALL THREADS
        U, V, W, added_turb_weight = hawc2farm.wsp_vec(
            Xmesh_m, Ymesh_m, Zmesh_m, return_added_turb_weights=True
        )
        if rank == 0:
            U_windfield, V_windfield, W_windfield = hawc2farm.wsp_vec(
                Xmesh_m,
                Ymesh_m,
                Zmesh_m,
                waked=False,
            )
        wake_xs, wake_ys, _ = hawc2farm.gather_tracer_loc()

        yaw_list_met = hawc2farm.gather_yaw_angle(global_coords=False)
        yaw_list_global = hawc2farm.gather_yaw_angle(global_coords=True)

        tilt_list_met = hawc2farm.gather_tilt_angle(global_coords=False)

        r_induction, induction = hawc2farm.gather_induction()

        if rank == 0:
            wake_x_trans, wake_y_trans = [], []
            for wake_x, wake_y in zip(wake_xs, wake_ys):
                wake_x = wake_x[~np.isnan(wake_x)]
                wake_y = wake_y[~np.isnan(wake_y)]
                blah = affine_trans(self.Ainv, np.vstack([wake_x, wake_y]))
                wake_x_trans.append(blah[0, :])
                wake_y_trans.append(blah[1, :])

            blah = affine_trans(
                self.Ainv,
                np.vstack([[x for x in xs], [y for y in ys]]),
            )

            out = {
                "turb_x": blah[0, :],
                "turb_y": blah[1, :],
                "turb_z": zs,
                "yaw": yaw_list_global,
                "yaw_met": yaw_list_met,
                "tilt_met": tilt_list_met,
                "R": hawc2farm.R,
                "wake_x": wake_x_trans,
                "wake_y": wake_y_trans,
                "windfield_x": Xs,
                "windfield_y": Ys,
                "Uamb": self.Uamb,
                "U": U,
                "V": V,
                "W": W,
                "U_windfield": U_windfield,
                "V_windfield": V_windfield,
                "W_windfield": W_windfield,
                "Added_turb_weight": added_turb_weight,
                "Ainv": self.Ainv,
                "r_induction": r_induction,
                "induction": induction,
            }
            with open(self.dumpdir / fn_out, "wb") as f:
                pickle.dump(out, f)


class DataDumper3D(LoggerBase):
    plot_ylim: list
    plot_zlim: list
    xslices: list
    plot_padding: float
    R: float
    zhub: float
    Uamb: float
    dumpdir: Path = Path("dump3D")
    Ainv: list
    log_interval: int
    dy: float
    dz: float

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.dumpdir.mkdir(parents=True, exist_ok=True)
        self.Ainv = np.array(self.Ainv)
        self.A = np.linalg.inv(self.Ainv)

    def log(self, hawc2farm):
        if hawc2farm.istep % self.log_interval != 0:
            return
        fn_out = f"datadump3D_{hawc2farm.istep:07}.pkl"

        # get current location of turbines
        x = hawc2farm.turbine.x
        xs = comm.allgather(x)

        y = hawc2farm.turbine.y
        ys = comm.allgather(y)

        z = hawc2farm.turbine.z
        zs = comm.allgather(z)

        ymin, ymax = (
            self.plot_ylim[0] - self.plot_padding,
            self.plot_ylim[1] + self.plot_padding,
        )
        zmin, zmax = (
            self.plot_zlim[0] - self.plot_padding,
            self.plot_zlim[1] + self.plot_padding,
        )

        Xs = self.xslices
        Ys = np.arange(ymin, ymax, self.dy)
        Zs = np.arange(zmin, zmax, self.dz)

        Xmesh_g, Ymesh_g, Zmesh_g = np.meshgrid(Xs, Ys, Zs, indexing="ij")

        blah = affine_trans(self.A, np.vstack([Xmesh_g.ravel(), Ymesh_g.ravel()]))
        Xmesh_m = blah[0, :].reshape(Xmesh_g.shape)
        Ymesh_m = blah[1, :].reshape(Ymesh_g.shape)
        Zmesh_m = Zmesh_g

        # MPI routines - MUST BE CALLED BY ALL THREADS
        U, V, W, added_turb_weight = hawc2farm.wsp_vec(
            Xmesh_m, Ymesh_m, Zmesh_m, return_added_turb_weights=True
        )

        if rank == 0:
            U_windfield, V_windfield, W_windfield = hawc2farm.wsp_vec(
                Xmesh_m,
                Ymesh_m,
                Zmesh_m,
                waked=False,
            )
        wake_xs, wake_ys, wake_zs = hawc2farm.gather_tracer_loc()
        yaw_list_met = hawc2farm.gather_yaw_angle(global_coords=False)
        yaw_list_global = hawc2farm.gather_yaw_angle(global_coords=True)
        rotor_azimuth_list = hawc2farm.gather_rotor_azimuth()
        r_induction, induction = hawc2farm.gather_induction()

        if rank == 0:
            wake_x_trans, wake_y_trans = [], []
            for wake_x, wake_y in zip(wake_xs, wake_ys):
                wake_x = wake_x[~np.isnan(wake_x)]
                wake_y = wake_y[~np.isnan(wake_y)]
                blah = affine_trans(self.Ainv, np.vstack([wake_x, wake_y]))
                wake_x_trans.append(blah[0, :])
                wake_y_trans.append(blah[1, :])

            blah = affine_trans(
                self.Ainv,
                np.vstack([[x for x in xs], [y for y in ys]]),
            )

            out = {
                "turb_x": blah[0, :],
                "turb_y": blah[1, :],
                "turb_z": zs,
                "yaw": yaw_list_global,
                "yaw_met": yaw_list_met,
                "R": hawc2farm.R,
                "wake_x": wake_x_trans,
                "wake_y": wake_y_trans,
                "wake_z": wake_zs[~np.isnan(wake_zs)],
                "windfield_x": Xs,
                "windfield_y": Ys,
                "windfield_z": Zs,
                "Uamb": self.Uamb,
                "U": U,
                "V": V,
                "W": W,
                "U_windfield": U_windfield,
                "V_windfield": V_windfield,
                "W_windfield": W_windfield,
                "Ainv": self.Ainv,
                "r_induction": r_induction,
                "induction": induction,
                "added_turb_weight": added_turb_weight,
                "rotor_azimuth": rotor_azimuth_list,
            }
            with open(self.dumpdir / fn_out, "wb") as f:
                pickle.dump(out, f)


class DefaultLogger(LoggerBase):
    logfile: Path
    log_interval: int

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.logfile.parent.mkdir(parents=True, exist_ok=True)
        logging.basicConfig(
            format="%(message)s",
            filename=self.logfile,
            level=logging.INFO,
            filemode="w",
        )
        self.t_start = time.time()

    def log(self, hawc2farm):
        if hawc2farm.istep % self.log_interval != 0:
            return
        if rank == 0:
            progress = (hawc2farm.istep / hawc2farm.Niter) * 100
            to_log = f"[{progress:2.2f}%] T: {hawc2farm.istep} ({time.time() - self.t_start:2.4f})"
            logging.info(to_log)
            self.t_start = time.time()

    def info(self, to_log):
        logging.info(to_log)


class RotorDumper(LoggerBase):
    dumpdir: Path = Path("rotordump")
    log_interval: int
    resolution: int = 30

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.dumpdir.mkdir(parents=True, exist_ok=True)

    def log(self, hawc2farm):
        if hawc2farm.istep % self.log_interval != 0:
            return

        T = hawc2farm.T
        fn_out = f"rotordump_{hawc2farm.istep:07}.pkl"

        # get current location of turbines
        x = hawc2farm.turbine.x
        xs = comm.allgather(x)

        y = hawc2farm.turbine.y
        ys = comm.allgather(y)

        if rank == 0:

            # rotor mesh
            Ys_rotor_base = np.linspace(
                -1.1 * hawc2farm.R, 1.1 * hawc2farm.R, self.resolution
            )
            Zs_rotor_base = hawc2farm.zhub + np.linspace(
                -1.1 * hawc2farm.R, 1.1 * hawc2farm.R, self.resolution
            )
            Ymesh_rotor_base, Zmesh_rotor_base = np.meshgrid(
                Ys_rotor_base, Zs_rotor_base
            )

            Xmesh_rotor = np.zeros((len(xs), self.resolution, self.resolution))
            Ymesh_rotor = np.zeros((len(xs), self.resolution, self.resolution))
            Zmesh_rotor = np.zeros((len(xs), self.resolution, self.resolution))
            for i, (x, y) in enumerate(zip(xs, ys)):
                Xmesh_rotor[i, :, :] = x
                Ymesh_rotor[i, :, :] = y + Ymesh_rotor_base
                Zmesh_rotor[i, :, :] = Zmesh_rotor_base

        else:
            Xmesh_rotor = np.empty(
                (len(xs), self.resolution, self.resolution), dtype=np.float64
            )
            Ymesh_rotor = np.empty(
                (len(xs), self.resolution, self.resolution), dtype=np.float64
            )
            Zmesh_rotor = np.empty(
                (len(xs), self.resolution, self.resolution), dtype=np.float64
            )

        # MPI routines - MUST BE CALLED BY ALL THREADS
        U_rotor, V_rotor, W_rotor = hawc2farm.wsp_vec(
            Xmesh_rotor, Ymesh_rotor, Zmesh_rotor, ignore_boundary_particle=True
        )

        if rank == 0:

            out = {
                "T": T,
                "turb_x": xs,
                "turb_y": ys,
                "U_rotor": U_rotor,
                "V_rotor": V_rotor,
                "W_rotor": W_rotor,
            }
            with open(self.dumpdir / fn_out, "wb") as f:
                pickle.dump(out, f)
