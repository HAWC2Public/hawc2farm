import numpy as np
import typing
from .utilities import MyBaseModel, load_mann_binary
from scipy import interpolate


class PartialWindfield(MyBaseModel):
    """
    Base class for Partial Windfield Models.
    """

    def step(self):
        pass

    def wsp(self, x, y, z):
        """
        Required function to be subclassed.
        """
        return np.array([0.0, 0.0, 0.0])


class NoShear(PartialWindfield):
    def wsp_vec(self, X, Y, Z):
        out = np.zeros((3, *X.shape))
        return out

    def wsp(self, x, y, z):
        return np.array([0.0, 0.0, 0.0])


class Constant(PartialWindfield):
    Uamb: float

    def wsp_vec(self, X, Y, Z):
        out = np.zeros((3, *X.shape))
        out[0] = self.Uamb
        return out

    def wsp(self, x, y, z):
        return np.array([self.Uamb, 0.0, 0.0])


class PowerLaw(PartialWindfield):
    Uref: float
    zref: float
    exp: float

    def wsp_vec(self, X, Y, Z):
        out = np.zeros((3, *X.shape))
        out[0] = self.shear(Z)
        return out

    def wsp(self, x, y, z):
        out = np.array([0.0, 0.0, 0.0])
        out[0] = self.shear(z)
        return out

    def shear(self, y):
        """
        Returns wind speed due to shear.
        """
        u = self.Uref * (y / self.zref) ** self.exp
        u = np.nan_to_num(u)
        return u


class LogLaw(PartialWindfield):
    Ufriction: float
    z0: float

    def wsp_vec(self, X, Y, Z):
        out = np.zeros((3, *X.shape))
        out[0] = self.shear(Z)
        return out

    def wsp(self, x, y, z):
        out = np.array([0.0, 0.0, 0.0])
        out[0] = self.shear(z)
        return out

    def shear(self, y):
        """
        Returns wind speed due to shear.
        """
        return self.Ufriction / 0.4 * np.log(y / self.z0)


class Step(PartialWindfield):
    amp: float
    loc: float

    def wsp(self, x, y, z):
        out = np.array([0.0, 0.0, 0.0])
        if x > self.loc:
            out[0] = self.amp
        else:
            out[0] = 0

        return out


class Mann(PartialWindfield):
    Uamb: float
    Nx: int
    Ny: int
    Nz: int
    Lx: float
    Ly: float
    Lz: float
    fn_u: str
    fn_v: str
    fn_w: str
    dt: float
    offset_init: float
    U_correction: float = 0
    fill_value: float = 0
    repeating = False

    def __init__(self, **params):
        super().__init__(**params)

        # Load box binary files.
        boxes = []
        for fn in [self.fn_u, self.fn_v, self.fn_w]:
            boxes.append(load_mann_binary(fn, (self.Nx, self.Ny, self.Nz)))

        # Generate spatial coordinates
        X = np.linspace(0, self.Lx, self.Nx)  # downstream
        Y = np.linspace(0, self.Ly, self.Ny)  # crossstream
        Z = np.linspace(0, self.Lz, self.Nz)

        # Generate final interpolation boxes
        self.boxes = []
        for box in boxes:
            self.boxes.append(
                interpolate.RegularGridInterpolator(
                    (X, Y, Z), box, fill_value=self.fill_value, bounds_error=False
                )
            )

        self.offset = self.Lx - self.offset_init

    def step(self):
        self.offset -= self.Uamb * self.dt

    def wsp(self, x, y, z):
        if self.repeating:
            x_ = (x + self.offset) % self.Lx
            y_ = y % self.Ly
            z_ = z % self.Lz
        else:
            x_ = x + self.offset
            y_ = y
            z_ = z

        out = np.array([box((x_, y_, z_)) for box in self.boxes])
        out[0] += self.U_correction
        return out

    def wsp_vec(self, X, Y, Z):
        if self.repeating:
            x_ = (X + self.offset) % self.Lx
            y_ = Y % self.Ly
            z_ = Z % self.Lz
        else:
            x_ = X + self.offset
            y_ = Y
            z_ = Z

        wsp = np.array([box((x_, y_, z_)) for box in self.boxes])
        wsp[0] += self.U_correction
        return wsp
