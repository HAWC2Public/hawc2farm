from .utilities import MyBaseModel



class ControllerBase(MyBaseModel):
    nturb: int

    def step(self, rews, power, thrust):
        pass

    @property
    def yaw_setpoints(self):
        return [0] * self.nturb


class none(ControllerBase):
    pass
