import numpy as np
from mpi4py import MPI
import pkg_resources

from .utilities import MyBaseModel
from . import (
    WakeSummationMethods,
    Windfields,
    TurbineModels,
    Controllers,
    Loggers,
    AddedTurbulence,
)
from jDWM.Wake import DynamicWake

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

for entry_point in pkg_resources.iter_entry_points("hawc2farm_plugin"):
    entry_point.load()


class HAWC2FarmBase(MyBaseModel):
    """
    A collection of wind turbines, windfield, and the dynamic wake model.
    This is an MPI object. Routines may initiate communication between MPI processes.
    """

    Niter: int
    R: float
    zhub: float
    Uamb: float
    dt: float
    max_particles: int
    dx_min: float
    windfield_update_interval: float

    # DWM parameters
    k1: float = 0.07
    k2: float = 0.008
    km1: float = 0.6
    km2: float = 0.35

    def __init__(self, **params):
        self.pre_init(**params)
        self.turbine.step()
        self.post_init()

    def pre_init(self, **params):
        super().__init__(**params["shared"])

        assert (
            len(params["turbine"]["HAWC2Turbine"]) == size
        ), f"Number of MPI processes ({size}) does not match number of turbines ({len(params['turbine']['HAWC2Turbine'])})."

        _class = TurbineModels.TurbineBase.get_subclass(list(params["turbine"])[0])
        self.turbine = _class(**params["turbine"][_class.__name__][rank])

        if rank == 0:
            _classes = [
                Windfields.PartialWindfield.get_subclass(x)
                for x in list(params["windfields"])
            ]
            self.windfields = {
                cls.__name__: cls(**params["windfields"][cls.__name__])
                for cls in _classes
            }
        else:
            self.windfields = None

        _class = WakeSummationMethods.SummationMethodBase.get_subclass(
            list(params["summation_method"])[0]
        )
        self.Summation_func = _class(**params["summation_method"][_class.__name__])

        _class = Controllers.ControllerBase.get_subclass(list(params["controller"])[0])
        self.controller = _class(**params["controller"][_class.__name__])

        _classes = [Loggers.LoggerBase.get_subclass(x) for x in list(params["logger"])]
        self.loggers = {
            cls.__name__: cls(**params["logger"][cls.__name__]) for cls in _classes
        }

        _class = AddedTurbulence.AddedTurbulenceBase.get_subclass(
            list(params["added_turbulence"])[0]
        )
        self.addedturbulence = _class(**params["added_turbulence"][_class.__name__])

        # MPI GATHER gridpoints
        self.Xmesh, self.Ymesh, self.Zmesh, self.grid_shape = self.gather_gridpoints()

        # MPI SCATTER interpolated windfield. U, V, W set to None if rank !=0.
        Usend, Vsend, Wsend = self.wsp_vec(
            self.Xmesh, self.Ymesh, self.Zmesh, waked=False
        )
        Urec = np.empty((self.grid_shape), dtype=np.float64)
        Vrec = np.empty((self.grid_shape), dtype=np.float64)
        Wrec = np.empty((self.grid_shape), dtype=np.float64)

        comm.Scatter(Usend, Urec, root=0)
        comm.Scatter(Vsend, Vrec, root=0)
        comm.Scatter(Wsend, Wrec, root=0)

        self.turbine.set_windfield(Urec, Vrec, Wrec)
        self.T = 0

    def post_init(self):
        self.turbine.post_init()
        # MPI GATHER induction.
        # r, induction_list = self.gather_induction()
        # Initialize wake objects.
        fc = self.Uamb / (4 * self.R)
        r, a = self.turbine.induction()
        x, y = self.turbine.x, self.turbine.y
        self.wake = DynamicWake(
            dt=self.dt,
            fc=fc,
            axial_induction_model="UserInput",
            x0=x / self.R,
            y0=y / self.R,
            z0=self.zhub / self.R,
            d_particle=1,
            TI=self.turbine.TI,
            axial_r=r / self.R,
            axial_a=a,
            max_particles=self.max_particles,
            dx_min=self.dx_min,
            k1=self.k1,
            k2=self.k2,
        )

        self.istep = 1
        self.update_mod = max(int(self.windfield_update_interval / self.dt), 1)

    def step(self):
        """
        Step forward in time by one time step.
        """

        self.T += self.dt

        if self.istep % self.update_mod == 0:
            # Interpolate windfield
            # MPI SCATTER windfield
            Usend, Vsend, Wsend = None, None, None
            if rank == 0:
                Xmesh = self.Xmesh
                Ymesh = self.Ymesh
                Zmesh = self.Zmesh
            else:
                Xmesh = np.empty((size, *self.grid_shape), dtype=np.float64)
                Ymesh = np.empty((size, *self.grid_shape), dtype=np.float64)
                Zmesh = np.empty((size, *self.grid_shape), dtype=np.float64)
            # if rank == 0:
            Usend, Vsend, Wsend = self.wsp_vec(
                Xmesh,  # only rank 0 has a real copy of Xmesh etc. all other ranks have empty array
                Ymesh,
                Zmesh,
                ignore_boundary_particle=True,
            )

            Urec = np.empty((self.grid_shape), dtype=np.float64)
            Vrec = np.empty((self.grid_shape), dtype=np.float64)
            Wrec = np.empty((self.grid_shape), dtype=np.float64)

            comm.Scatter(Usend, Urec, root=0)
            comm.Scatter(Vsend, Vrec, root=0)
            comm.Scatter(Wsend, Wrec, root=0)

            self.turbine.set_windfield(Urec, Vrec, Wrec)

        offset = self.Uamb * self.dt * (self.istep % self.update_mod)
        self.turbine.set_offset(offset)

        self.turbine.step()

        # GET INDUCTION
        _, a = self.turbine.induction()

        # GET WIND FIELD AT WAKE POINTS
        X_part, Y_part, Z_part = self.gather_tracer_loc()

        if rank == 0:
            Usend, Vsend, Wsend = self.wsp_vec(X_part, Y_part, Z_part, waked=False)
            Usend, Vsend, Wsend = Usend / self.R, Vsend / self.R, Wsend / self.R
        else:
            Usend, Vsend, Wsend = None, None, None

        Urec = np.empty(self.max_particles, dtype=np.float64)
        Vrec = np.empty(self.max_particles, dtype=np.float64)
        Wrec = np.empty(self.max_particles, dtype=np.float64)
        comm.Scatter(Usend, Urec, root=0)
        comm.Scatter(Vsend, Vrec, root=0)
        comm.Scatter(Wsend, Wrec, root=0)

        # step wakes and windfield.
        yaw = self.turbine.yaw
        tilt = self.turbine.tilt
        self.step_wakes(a, Urec, Vrec, Wrec, yaw=yaw, tilt=tilt)
        if rank == 0:
            for _, windfield in self.windfields.items():
                windfield.step()

        self.addedturbulence.step()

        cont_rews, cont_power, cont_thrust = self.gather_control_inputs()
        # controller action here-ish
        if rank == 0:
            self.controller.step(cont_rews, cont_power, cont_thrust)
            data = self.controller.yaw_setpoints
        else:
            data = None
        data = comm.scatter(data, root=0)
        self.turbine.set_yaw_setpoint(data)

        for _, logger in self.loggers.items():
            logger.log(self)

        self.istep += 1

    def running(self):
        return self.istep <= self.Niter

    def step_wakes(self, induction, U, V, W, yaw=0, tilt=0):
        """
        PARALLEL.
        Perform a waketime step.
        args:
        induction (np.array): Axisymmetric xial induction profile as a function of radius.
        U (np.array): 1D array containing the U velocity at the location of each wake particle of this wake.
        V (np.array): 1D array containing the V velocity at the location of each wake particle of this wake.
        W (np.array): 1D array containing the W velocity at the location of each wake particle of this wake.
        yaw (float): Current yaw angle of the rotor (degrees)
        tilt (float): Current tilt angle of the rotor (degrees)
        """
        U = U[~np.isnan(U)]
        V = -V[~np.isnan(V)]  # Note the sign change in lateral velocity
        W = W[~np.isnan(W)]

        self.wake.x0 = self.turbine.x_pos / self.R
        self.wake.y0 = self.turbine.y_pos / self.R
        self.wake.z0 = self.turbine.z_pos / self.R
        self.wake.step(zip(U, V, W), axial_a=induction, yaw=-yaw, tilt=tilt)

    def gather_tracer_loc(self):
        X = np.empty(self.max_particles)
        Y = np.empty(self.max_particles)
        Z = np.empty(self.max_particles)
        X.fill(np.nan)
        Y.fill(np.nan)
        Z.fill(np.nan)

        for i, particle in enumerate(self.wake.particles):
            X[i] = particle.x
            Y[i] = particle.y
            Z[i] = particle.z
        X *= self.R
        Y *= self.R
        Z *= self.R

        Xs, Ys, Zs = None, None, None
        if rank == 0:
            Xs = np.empty((size, self.max_particles), dtype=np.float64)
            Ys = np.empty((size, self.max_particles), dtype=np.float64)
            Zs = np.empty((size, self.max_particles), dtype=np.float64)

        comm.Gather(X, Xs, root=0)
        comm.Gather(Y, Ys, root=0)
        comm.Gather(Z, Zs, root=0)

        return Xs, Ys, Zs

    def gather_gridpoints(self):
        Xs, Ys, Zs = self.turbine.gridpoints()
        Xmesh, Ymesh, Zmesh = np.meshgrid(Xs, Ys, Zs, indexing="ij")

        Xmesh_list, Ymesh_list, Zmesh_list = None, None, None
        if rank == 0:
            Xmesh_list = np.empty((size, *Xmesh.shape), dtype=np.float64)
            Ymesh_list = np.empty((size, *Xmesh.shape), dtype=np.float64)
            Zmesh_list = np.empty((size, *Xmesh.shape), dtype=np.float64)
        comm.Gather(Xmesh, Xmesh_list, root=0)
        comm.Gather(Ymesh, Ymesh_list, root=0)
        comm.Gather(Zmesh, Zmesh_list, root=0)

        return Xmesh_list, Ymesh_list, Zmesh_list, Xmesh.shape

    def gather_turbine_subboxes(self):
        """Returns the HAWC2 subbox wind field of all turbines to rank 0."""
        Urec, Vrec, Wrec = None, None, None
        if rank == 0:
            Urec = np.empty((size, *self.grid_shape), dtype=np.float64)
            Vrec = np.empty((size, *self.grid_shape), dtype=np.float64)
            Wrec = np.empty((size, *self.grid_shape), dtype=np.float64)

        Usend, Vsend, Wsend = self.turbine.get_windfield()
        comm.Gather(Usend, Urec, root=0)
        comm.Gather(Vsend, Vrec, root=0)
        comm.Gather(Wsend, Wrec, root=0)

        return Urec, Vrec, Wrec

    def gather_induction(self):
        """
        MPI ROUTINE: Must be called by ALL MPI processes.
        Gathers the rotor induction profile from all turbines.
        """
        r, a = self.turbine.induction()
        induction_list = None
        if rank == 0:
            induction_list = np.empty((size, len(r)), dtype=np.float64)

        comm.Gather(a, induction_list, root=0)

        return r, induction_list

    def gather_yaw_angle(self, global_coords=False):
        """
        MPI ROUTINE: Must be called by ALL MPI processes.
        Gathers the yaw angle of each turbine (degrees).
        """
        if global_coords:
            yaw = self.turbine.yaw_global
        else:
            yaw = self.turbine.yaw
        yaw_list = comm.gather(yaw, root=0)

        return yaw_list

    def gather_tilt_angle(self, global_coords=False):
        """
        MPI ROUTINE: Must be called by ALL MPI processes.
        Gathers the tilt angle of each turbine (degrees).
        """
        if global_coords:
            raise NotImplementedError
        else:
            tilt = self.turbine.tilt
        tilt_list = comm.gather(tilt, root=0)

        return tilt_list

    def gather_rotor_azimuth(self):
        """
        MPI ROUTINE: Must be called by ALL MPI processes.
        Gathers the rotor azimuth angle of each turbine (degrees).
        """
        azimuth = self.turbine.get_rotor_azimuth()
        azimuth_list = comm.gather(azimuth, root=0)

        return azimuth_list

    def gather_control_inputs(self):
        """
        MPI ROUTINE: Must be called by ALL MPI processes. Gathers measurements
        from the turbine to be sent to the wind farm controller.
        """
        # [REWS(m/s), Power(kW), Thrust (kN)]
        signal = self.turbine.control_signal()
        signal_vec = None
        if rank == 0:
            signal_vec = np.empty((size, len(signal)), dtype=np.float64)

        comm.Gather(signal, signal_vec, root=0)

        rews, power, thrust = None, None, None
        if rank == 0:
            # separate signal into different channels
            signal_vec = np.array(signal_vec)
            rews = signal_vec[:, 0]
            power = signal_vec[:, 1]
            thrust = signal_vec[:, 2]

        return rews, power, thrust

    def wsp_vec(self, Xmesh, Ymesh, Zmesh, waked=True, ignore_boundary_particle=False, return_added_turb_weights=False):
        """
        MPI ROUTINE: Must be called by ALL MPI processes.
        Only rank zero should have the values for Xmesh, Ymesh, Zmesh.
        All other ranks should have an empty numpy array of the correct shape.
        """

        if rank == 0:
            out = np.zeros((3, *Xmesh.shape))

            for windfield in self.windfields.values():
                out += windfield.wsp_vec(Xmesh, Ymesh, Zmesh)
            U, V, W = out
        else:
            U, V, W = None, None, None

        if waked:
            comm.Bcast(Xmesh, root=0)
            comm.Bcast(Ymesh, root=0)
            comm.Bcast(Zmesh, root=0)
            U_waked_norm, U_grad_norm = self.wake.wsp(
                Xmesh / self.R,
                Ymesh / self.R,
                Zmesh / self.R,
                ignore_boundary_particle=ignore_boundary_particle,
                gradient=True,
            )

            added_turb_weight = (
                np.abs(1 - U_waked_norm) * self.km1 + np.abs(U_grad_norm) * self.km2
            )
            isoU, _, _ = self.addedturbulence.wsp_vec(Xmesh, Ymesh, Zmesh)
            U_waked = U_waked_norm * self.Uamb + isoU * added_turb_weight

            U_waked_list = None
            if rank == 0:
                U_waked_list = np.empty((size, *Xmesh.shape), dtype=np.float)

            comm.Gather(U_waked, U_waked_list, root=0)

            if rank == 0:
                U = self.Summation_func(U, self.Uamb, U_waked_list)
                U[U < 0] = 0.01

            if return_added_turb_weights:
                turb_weight_list = None
                if rank == 0:
                    turb_weight_list = np.empty((size, *Xmesh.shape), dtype=np.float)

                comm.Gather(added_turb_weight, turb_weight_list, root=0)

                turb_weight_summed = None
                if rank == 0:
                    turb_weight_summed = sum(turb_weight_list)
                return U, V, W, turb_weight_summed

        return U, V, W

