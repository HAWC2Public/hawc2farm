import toml
from pathlib import Path
import pkg_resources

from rich import print
from rich.tree import Tree


STATUS_STRING = {
    "provided": "[bold green]✓",
    "shared": "[bold green]✓ [italic](shared)",
    "default": "[bold green]✓ [italic](default)",
    "missing": "[bold red]✖ MISSING",
    "unused": "[bold yellow]![italic] provided but not needed",
}


def load_input(fn):
    fn = Path(fn)
    assert fn.exists(), f"fn cannot be found."

    return toml.load(fn)


def validate_models(input: dict):
    from . import (
        WakeSummationMethods,
        Windfields,
        TurbineModels,
        Controllers,
        Loggers,
        AddedTurbulence,
    )

    for entry_point in pkg_resources.iter_entry_points("hawc2farm_plugin"):
        entry_point.load()

    model_bases = {
        "summation_method": WakeSummationMethods.SummationMethodBase,
        "windfields": Windfields.PartialWindfield,
        "turbine": TurbineModels.TurbineBase,
        "controller": Controllers.ControllerBase,
        "logger": Loggers.LoggerBase,
        "added_turbulence": AddedTurbulence.AddedTurbulenceBase,
    }
    out = {}

    for key, baseclass in model_bases.items():
        subclass_names = [x.__name__ for x in baseclass.all_subclasses()]
        out[key] = {key: True if key in subclass_names else False for key in input[key]}

    return out


def isvalid(input: dict):
    validation_models = validate_models(input)
    models_are_valid = all(
        all(x for x in d.values()) for d in validation_models.values()
    )

    _, validation_fields = expand_input(input)
    valid_fields = True
    for model_type in [
        "summation_method",
        "windfields",
        "controller",
        "logger",
        "added_turbulence",
    ]:
        valid_fields = valid_fields and all(
            all(x != "missing" for x in d.values())
            for d in validation_fields[model_type].values()
        )

    for model_list_type in ["turbine"]:
        for model in validation_fields[model_list_type]:
            for instance in validation_fields[model_list_type][model]:
                valid_fields = valid_fields and all(
                    x != "missing" for x in instance.values()
                )
    return models_are_valid and valid_fields


def get_model_requirements(input: dict):
    from . import (
        WakeSummationMethods,
        Windfields,
        TurbineModels,
        Controllers,
        Loggers,
        AddedTurbulence,
    )

    for entry_point in pkg_resources.iter_entry_points("hawc2farm_plugin"):
        entry_point.load()

    model_bases = {
        "summation_method": WakeSummationMethods.SummationMethodBase,
        "windfields": Windfields.PartialWindfield,
        "turbine": TurbineModels.TurbineBase,
        "controller": Controllers.ControllerBase,
        "logger": Loggers.LoggerBase,
        "added_turbulence": AddedTurbulence.AddedTurbulenceBase,
    }
    out = {}

    for key, baseclass in model_bases.items():
        out[key] = {}
        for model, fields in input[key].items():
            out[key][model] = list(baseclass.get_subclass(model).__fields__.values())

    return out


def check_fields_single(to_check: dict, fields: list, shared: dict):
    out = {}
    info = {}
    for field in fields:
        if field.name in to_check:
            out[field.name] = to_check[field.name]
            info[field.name] = "provided"
        elif field.name in shared:
            out[field.name] = shared[field.name]
            info[field.name] = "shared"
        elif field.required == False:
            out[field.name] = field.default
            info[field.name] = "default"
        else:
            info[field.name] = "missing"

    # get 'provided but not needed' fields.
    difference = set(to_check.keys()) - set(out.keys())
    for field_name in difference:
        info[field_name] = "unused"

    return out, info


def expand_input(input: dict):
    out = {}
    info = {}
    shared = input["shared"]
    out["shared"] = shared

    requirements = get_model_requirements(input)
    for model_type in [
        "summation_method",
        "windfields",
        "controller",
        "logger",
        "added_turbulence",
    ]:
        out[model_type] = {}
        info[model_type] = {}
        for model, values in input[model_type].items():
            _out, _info = check_fields_single(
                values, requirements[model_type][model], shared
            )
            out[model_type][model] = _out
            info[model_type][model] = _info

    for model_list_type in ["turbine"]:
        out[model_list_type] = {}
        info[model_list_type] = {}
        for model, instances in input[model_list_type].items():
            out[model_list_type][model] = []
            info[model_list_type][model] = []
            for instance in instances:
                _out, _info = check_fields_single(
                    instance, requirements[model_list_type][model], shared
                )
                out[model_list_type][model].append(_out)
                info[model_list_type][model].append(_info)

    return out, info


def pretty_print_info(info):

    for model_type, models in info.items():
        tree = Tree(f"[bold red]{model_type}")
        for model, fields in models.items():

            if isinstance(fields, list):
                for instance in fields:
                    cls_tree = tree.add(f"[italic bold green]{model}")
                    for field, status in instance.items():
                        cls_tree.add(f"{field} {STATUS_STRING[status]}")
            elif isinstance(fields, dict):
                cls_tree = tree.add(f"[italic bold green]{model}")
                for field, status in fields.items():
                    cls_tree.add(f"{field} {STATUS_STRING[status]}")
        print()
        print(tree)
