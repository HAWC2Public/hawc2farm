from pathlib import Path
import pkg_resources
import click
import toml
from rich import print as pprint
from rich.tree import Tree
from . import Status

CONFIG_PATH = Path(__file__).parent.parent / ".config.toml"


@click.group()
def cli():
    """
    \b
     _   _    ___        ______ ____  _____
    | | | |  / \ \      / / ___|___ \|  ___|_ _ _ __ _ __ ___
    | |_| | / _ \ \ /\ / / |     __) | |_ / _` | '__| '_ ` _ \\
    |  _  |/ ___ \ V  V /| |___ / __/|  _| (_| | |  | | | | | |
    |_| |_/_/   \_\_/\_/  \____|_____|_|  \__,_|_|  |_| |_| |_|
    Aeroelastic wind farm simulator.
    Author: Jaime Liew (jyli@dtu.dk)
    """
    if not CONFIG_PATH.exists():
        config_init(CONFIG_PATH)


@cli.command(help="Display available models.")
@click.option("-v", "--verbose", count=True)
def avail(verbose):
    from . import (
        Loggers,
        Windfields,
        TurbineModels,
        WakeSummationMethods,
        Controllers,
        AddedTurbulence,
    )

    for entry_point in pkg_resources.iter_entry_points("hawc2farm_plugin"):
        entry_point.load()

    model_types = {
        "turbine": TurbineModels.TurbineBase,
        "windfields": Windfields.PartialWindfield,
        "summation_method": WakeSummationMethods.SummationMethodBase,
        "controller": Controllers.ControllerBase,
        "logger": Loggers.LoggerBase,
        "added_turbulence": AddedTurbulence.AddedTurbulenceBase,
    }

    for model_type, baseclass in model_types.items():
        tree = Tree(f"[bold red]{model_type}")
        for cls in baseclass.all_subclasses():
            cls_tree = tree.add(f"[italic bold green]{cls.__name__}")
            if verbose:
                for field in cls.__fields__.values():
                    if field.default is not None:
                        desc = f"{field.name}[green] = {field.default} [bright_cyan italic]({field.type_.__name__})"
                    else:
                        desc = (
                            f"{field.name} [bright_cyan italic]({field.type_.__name__})"
                        )
                    cls_tree.add(desc)
        pprint()
        pprint(tree)


@cli.command(help="Run HAWC2Farm simulation from input file.")
@click.argument("filename", type=click.Path(exists=True, dir_okay=False))
def run(filename):
    simulate_h2f(filename)


@cli.command(help="Validate HAWC2Farm input file (or directory).")
@click.argument("filename", type=click.Path(exists=True))
@click.option("-v", "--verbose", count=True)
def validate(filename, verbose):
    """
    list all required parameters. Find parameters which aren't defined or which
    are defined but aren't used.
    """
    from HAWC2Farm import Input

    filename = Path(filename)

    if filename.is_file():
        input = Input.load_input(filename)
        _, info = Input.expand_input(input)
        if verbose:
            Input.pretty_print_info(info)
        else:
            print(filename, "VALID" if Input.isvalid(input) else "INVALID")

    elif filename.is_dir():
        for fn in filename.iterdir():
            input = Input.load_input(fn)
            _, info = Input.expand_input(input)
            if verbose:
                Input.pretty_print_info(info)
            else:
                print(fn, "VALID" if Input.isvalid(input) else "INVALID")


@cli.command(help="Show simulation status of input files (or directories).")
@click.argument("h2f_files", nargs=-1, type=click.Path(exists=True))
@click.option(
    "--long", "-l", is_flag=True, default=False, help="Display in long-format."
)
@click.option(
    "--sort",
    type=click.Choice(["progress", "filename"], case_sensitive=False),
    default="filename",
    help="Sort long-formatted statuses by progress or filename.",
)
def status(h2f_files, long, sort):
    Status.status(h2f_files, long, sort)


@cli.command(help="Configure HAWC2Farm.")
@click.option(
    "--set-hawc2", type=click.Path(exists=True), help="Set path to HAWC2 shared object."
)
def config(set_hawc2):
    params = toml.load(CONFIG_PATH)

    if set_hawc2:
        params["hawc2"] = set_hawc2

    with open(CONFIG_PATH, "w") as f:
        toml.dump(params, f)
    pprint(params)


def simulate_h2f(fn):
    from . import Input
    from HAWC2Farm.HAWC2Farm import HAWC2FarmBase

    input = Input.load_input(fn)
    assert Input.isvalid(input), f"{fn} is invalid."
    input, _ = Input.expand_input(input)
    # hawc2farm_class = construct_HAWC2Farm_class(input)
    hawc2farm = HAWC2FarmBase(**input)

    while hawc2farm.running():
        hawc2farm.step()


def config_init(config_path):
    params_init = {"hawc2": ""}
    with open(config_path, "w") as f:
        toml.dump(params_init, f)


if __name__ == "__main__":
    cli()
