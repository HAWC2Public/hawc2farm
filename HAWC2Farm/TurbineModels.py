"""
Author: Jaime Liew (jyli@dtu.dk)
Requires:
    - PyHawc2
    - .utilities

Used by:
    - HAWC2Farm.HAWC2Farm
The HAWC2 turbine instance class. Each HAWC2Turbine instance connects to a
single HAWC2 simulation. One HAWC2Turbine instance is spawned per MPI process.
"""
import numpy as np
from pathlib import Path
import toml
from .utilities import MyBaseModel
from typing import Optional

CONFIG_PATH = Path(__file__).parent.parent / ".config.toml"
CONFIG = toml.load(CONFIG_PATH)


class TurbineBase(MyBaseModel):
    pass


class HAWC2Turbine(TurbineBase):
    ID: str
    htc_fn: str
    x: float
    y: float
    z: float
    r_max: float
    Nr: int
    R: float
    Uamb: float
    dt: float
    TI: float
    yaw_fc: Optional[float]
    tilt_fc: Optional[float]
    position_fc: Optional[float]

    def __init__(self, **params):
        super().__init__(**params)
        from pyhawc2.pyhawc2 import PyHawc2

        if "hawc2" in CONFIG:
            self.pyhaw = PyHawc2(path_input=self.htc_fn, path_solv=CONFIG["hawc2"])
        else:
            self.pyhaw = PyHawc2(path_input=self.htc_fn, path_solv=None)
        self.pyhaw.py2main.init_hawc2()
        print("\tSyncronization step (finishing allocations, etc.)")
        self.pyhaw.py2main.syn_hawc2()
        self.Xs, self.Ys, self.Zs = self.pyhaw.py2hawc2farm.get_hawc2farm_gridpoints()
        self.r_grid_reg = np.linspace(0, self.r_max, self.Nr)

        # Calculate low pass filter coefficient based on DWM cut off frequency
        # for yaw angle smoothing.
        if self.yaw_fc is None:
            fc = self.Uamb / (4 * self.R)
        else:
            fc = self.yaw_fc
        wc = fc * self.dt / (2 * np.pi)
        self.alpha_yaw = np.cos(wc) - 1 + np.sqrt(np.cos(wc) ** 2 - 4 * np.cos(wc) + 3)

        # Calculate low pass filter coefficient based on DWM cut off frequency
        # for tilt angle smoothing.
        if self.tilt_fc is None:
            fc = self.Uamb / (4 * self.R)
        else:
            fc = self.tilt_fc
        wc = fc * self.dt / (2 * np.pi)
        self.alpha_tilt = np.cos(wc) - 1 + np.sqrt(np.cos(wc) ** 2 - 4 * np.cos(wc) + 3)

        # Calculate low pass filter coefficient for rotor position smoothing (default = None).
        if self.position_fc is None:
            self.alpha_pos = 1
        else:
            fc = self.position_fc
            wc = fc * self.dt / (2 * np.pi)
            self.alpha_pos = np.cos(wc) - 1 + np.sqrt(np.cos(wc) ** 2 - 4 * np.cos(wc) + 3)

        # Initialise state variables
        self.yaw = 0
        self.yaw_global = 0
        self.tilt = 0
        self.x_pos = self.x
        self.y_pos = self.y
        self.z_pos = self.z

    def post_init(self):
        """
        Post initialization to be run after the first iteration.
        """
        self.azi_grid, self.r_grid = self.pyhaw.py2hawc2farm.get_bem_grid()

    def step(self):
        self.pyhaw.py2main.step_predict_hawc2()
        self.pyhaw.py2main.step_correct_hawc2()

        # Update yaw angle with low pass filter.
        yaw_instant = self.get_yaw()
        self.yaw += self.alpha_yaw * (yaw_instant - self.yaw)

        # Update tilt angle with low pass filter.
        tilt_instant = self.get_tilt()
        self.tilt += self.alpha_tilt * (tilt_instant - self.tilt)

        self.yaw_global = self.get_yaw(global_coords=True)

        # Update rotor position
        x_local, y_local, z_local = self.get_rotor_pos()
        pos_instant_x = self.x + x_local
        pos_instant_y = self.y + y_local
        pos_instant_z = z_local 

        # Update rotor position with low pass filter.
        self.x_pos += self.alpha_pos * (pos_instant_x - self.x_pos)
        self.y_pos += self.alpha_pos * (pos_instant_y - self.y_pos)
        self.z_pos += self.alpha_pos * (pos_instant_z - self.z_pos)
        
        # NOTE: self.z is not added to z_local as self.z is currently defined as
        # the global fixed position of the rotor. Perhaps self.z should be
        # defined as the HAWC2 global zero location (i.e. self.z = 0), but this
        # would require All HAWC2Farm simulations to be defined this way and
        # would break some older simulation setups.

    def gridpoints(self, FOR="global"):
        """
        Returns the coordinates of the wind field grid points in either global
        or local frame of reference (FOR).
        """
        if FOR == "global":
            return self.Xs + self.x, self.Ys + self.y, self.Zs + self.z
        elif FOR == "local":
            return self.Xs, self.Ys, self.Zs
        else:
            assert FOR in ["global", "local"]

    def set_windfield(self, U, V, W):
        """
        Sets the HAWC2 windfield subbox. U, V, and W are arrays of shape Nx, Ny,
        Nz corresponding to the wind speeds at the points at self.gridpoints().
        """
        self.pyhaw.py2hawc2farm.set_hawc2farm_windfieldu(np.array(U, order="F"))
        self.pyhaw.py2hawc2farm.set_hawc2farm_windfieldv(np.array(V, order="F"))
        self.pyhaw.py2hawc2farm.set_hawc2farm_windfieldw(np.array(W, order="F"))

    def get_windfield(self):
        """
        Gets the HAWC2 windfield subbox. U, V, and W are arrays of shape Nx, Ny,
        Nz corresponding to the wind speeds at the points at self.gridpoints().
        """
        U = self.pyhaw.py2hawc2farm.get_hawc2farm_windfieldu()
        V = self.pyhaw.py2hawc2farm.get_hawc2farm_windfieldv()
        W = self.pyhaw.py2hawc2farm.get_hawc2farm_windfieldw()

        return U, V, W

    def set_offset(self, offset):
        self.pyhaw.py2hawc2farm.set_hawc2farm_offset(offset)

    def set_yaw_setpoint(self, setpoint):
        self.pyhaw.py2hawc2farm.set_yaw_setpoint(setpoint)

    def set_derate_setpoint(self, setpoint):
        self.pyhaw.py2hawc2farm.set_derate_setpoint(setpoint)

    def power(self):
        """
        Returns the aerodynamic power output of the turbine (kW).
        """
        p = self.pyhaw.py2hawc2farm.get_aero_power()
        return p

    def thrust(self):
        """
        Returns the aerodynamic thrust of the turbine (kN).
        """
        T = self.pyhaw.py2hawc2farm.get_aero_thrust()
        return T

    def get_hub_wsp(self):
        """
        Returns the instantaneous wind speed vector (u, v, w) at the rotor hub
        in rotor coordinates.
        """
        out = self.pyhaw.py2hawc2farm.get_hub_wsp()
        return out

    def get_rotor_azimuth(self):
        """
        Returns the rotor azimuth angle [deg].
        """
        out = self.pyhaw.py2hawc2farm.get_rotor_azimuth()
        return out

    def rews(self):
        """
        Returns the rotor effective wind speed in rotor coordinates.
        """
        out = self.pyhaw.py2hawc2farm.get_rotor_avg_wsp()
        return out

    def induction(self):
        """
        Returns the axial induction profile as a function of radial position.
        """
        axial_a = self.pyhaw.py2hawc2farm.get_induction()
        axial_a[-1] = 0

        # interpolate on regular grid
        axial_a_reg = np.interp(self.r_grid_reg, self.r_grid, axial_a)
        return self.r_grid_reg, axial_a_reg

    def get_yaw(self, global_coords=False):
        """
        Returns the instantaneous yaw angle of the turbine (in degrees).
        if global_coords==False, returns yaw relative to free wind.
        if global_coords==True, returns yaw relative to global coordinates
        """
        if global_coords:
            yaw = self.pyhaw.py2hawc2farm.get_yaw_global()
        else:
            yaw = self.pyhaw.py2hawc2farm.get_yaw()
        return yaw

    def get_tilt(self, global_coords=False):
        """
        Returns the instantaneous tilt angle of the turbine (in degrees).
        """
        if global_coords:
            raise NotImplementedError
        else:
            tilt = self.pyhaw.py2hawc2farm.get_tilt()
        return tilt

    def get_rotor_pos(self):
        """
        Returns the instantaneous rotor position in global coordinates.
        """

        x_h2, y_h2, z_h2 = self.pyhaw.py2hawc2farm.get_rotor_pos()

        # Convert HAWC2 global coordinate sto HAWC2Farm global coordinates
        x = y_h2
        y = x_h2
        z = -z_h2

        return x, y, z
    
    def get_rotor_pos_fixed(self):
        """
        Returns the FIXED rotor position in global coordinates.
        """

        x, y, z = self.pyhaw.py2hawc2farm.get_rotor_pos_fixed()
        return x, y, z

    def control_signal(self):
        """
        Returns an array of values from the state of the turbine. To be used as an
        input to a wind farm controller.
        """
        return np.array([self.rews(), self.power(), self.thrust()])
