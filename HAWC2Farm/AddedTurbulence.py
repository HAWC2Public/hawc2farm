import numpy as np
import typing
from .utilities import MyBaseModel, load_mann_binary
from scipy import interpolate


class AddedTurbulenceBase(MyBaseModel):
    """
    Base class for Added Turbulence Models.
    """

    def step(self):
        pass

    def wsp_vec(self, X, Y, Z):
        out = np.zeros((3, *X.shape))
        return out

    def wsp(self, x, y, z):
        return np.array([0.0, 0.0, 0.0])


class none(AddedTurbulenceBase):
    pass


class Isotropic(AddedTurbulenceBase):
    Uamb: float
    dt: float
    Nx: int
    Ny: int
    Nz: int
    Lx: float
    Ly: float
    Lz: float
    fn_u: str
    fn_v: str
    fn_w: str

    def __init__(self, **params):
        super().__init__(**params)

        # Load box binary files.
        boxes = []
        for fn in [self.fn_u, self.fn_v, self.fn_w]:
            boxes.append(
                load_mann_binary(
                    fn, (self.Nx, self.Ny, self.Nz)
                )
            )

        # Generate spatial coordinates
        X = np.linspace(0, self.Lx, self.Nx)  # downstream
        Y = np.linspace(0, self.Ly, self.Ny)  # crossstream
        Z = np.linspace(0, self.Lz, self.Nz)

        # Generate final interpolation boxes
        self.boxes = []
        for box in boxes:
            self.boxes.append(
                interpolate.RegularGridInterpolator(
                    (X, Y, Z), box, fill_value=0, bounds_error=False
                )
            )

        self.offset = 0

    def step(self):
        self.offset -= self.Uamb * self.dt

    def wsp(self, x, y, z):
        x, y, z = (
            (x + self.offset) % self.Lx,
            y % self.Ly,
            z % self.Lz,
        )

        out = np.array([box((x + self.offset, y, z)) for box in self.boxes])
        return out

    def wsp_vec(self, X, Y, Z):
        X, Y, Z = (
            (X + self.offset) % self.Lx,
            Y % self.Ly,
            Z % self.Lz,
        )
        wsp = np.array([box((X, Y, Z)) for box in self.boxes])
        return wsp
