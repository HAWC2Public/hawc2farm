import numpy as np
from .utilities import MyBaseModel


def product(X):
    out = 1
    for x in X:
        out *= x
    return out


class SummationMethodBase(MyBaseModel):
    pass


class Linear(SummationMethodBase):
    def __call__(self, U, U_amb, U_waked):
        """
        Args:
        U_amb (float): Wind speed at location.
        U_waked (list): list of wind speeds at location due to a single wake,
        .
        Returns:
        U_out (float): The summed wind speed.
        """
        U_out = U + sum(U_w - U_amb for U_w in U_waked)
        return U_out


class Quadratic(SummationMethodBase):
    def __call__(self, U, U_amb, U_waked):
        U_out = U - np.sqrt(sum((U_amb - U_w) ** 2 for U_w in U_waked))
        return U_out


class Geometric(SummationMethodBase):
    def __call__(self, U, U_amb, U_waked):
        U_out = U * product([U_w / U_amb for U_w in U_waked])
        return U_out


class Dominant(SummationMethodBase):
    def __call__(self, U, U_amb, U_waked):
        U_out = U + np.min(U_waked, axis=0) - U_amb
        return U_out
