from pathlib import Path
import time
import re
import toml
import os
from tqdm import tqdm
import pandas as pd
from concurrent.futures import ProcessPoolExecutor
from rich import print
from rich.progress import ProgressBar
from rich.table import Table


STATUS_COLOR = {
    "AWAITING": "grey58",
    "COMPLETE": "bright_cyan",
    "HALTED": "red",
    "RUNNING": "green",
}


pattern = re.compile("\[(.*)%\] T.*")


def reverse_readline(filename, buf_size=8192):
    """
    A generator that returns the lines of a file in reverse order
    https://stackoverflow.com/questions/2301789/how-to-read-a-file-in-reverse-order/23646049#23646049

    """
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split("\n")
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first
                if buffer[-1] != "\n":
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


def make_filelist(directories):
    out = []
    for directory in directories:
        directory = Path(directory)

        if not directory.is_dir():
            out.append(directory)
        else:
            out += list(directory.iterdir())

    return out


def h2f_2_status(file_list):

    with ProcessPoolExecutor() as ex:
        out = ex.map(h2f_2_status_single, file_list)

    df = pd.DataFrame(out, columns=["filename", "status", "progress"])
    return df


def h2f_2_status_single(fn: Path):
    params = toml.load(open(fn, "r"))
    logfile = Path(params["logger"]["DefaultLogger"]["logfile"])
    if not logfile.exists():
        status, progress = "AWAITING", 0

    else:
        t_since_modified = time.time() - logfile.stat().st_mtime

        for line in reverse_readline(logfile):
            res = pattern.match(line)
            if res is not None:
                try:
                    progress = float(res.groups()[0])
                except:
                    progress = 0
                break
        else:
            progress = 0

        if progress == 100:
            status, progress = "COMPLETE", 100
        elif t_since_modified > 60:
            status, progress = "HALTED", progress
        else:
            status, progress = "RUNNING", progress

    return fn.as_posix(), status, progress


def pretty_print_progress(status, sort_by):

    status = status.sort_values(sort_by, ascending=False)

    for _, x in status.iterrows():
        _status = f"[{STATUS_COLOR[x.status]}]{x.status:<9}[/{STATUS_COLOR[x.status]}]"
        _progress = f" [bold magenta] {x.progress:2.1f}% "
        progress_bar = ProgressBar(width=20, completed=x.progress)
        print(_status, progress_bar, _progress, x.filename, "\n", end="")


def pretty_print_summary(status):
    df_piv = status.pivot_table(
        index="status", values="progress", aggfunc=("count", "mean")
    )

    table = Table()
    table.add_column("Status", justify="right", style="bold cyan")
    table.add_column("Count", justify="center", style="bright_white")
    table.add_column("Progress", style="bright_white", justify="center")
    table.add_column("Complete", style="bright_white", justify="center")

    for i, (_status, row) in enumerate(df_piv.iterrows()):
        end_section = i == len(df_piv) - 1
        table.add_row(
            _status,
            f"{int(row['count'])}",
            ProgressBar(width=50, completed=row["mean"]),
            f"{row['mean']:2.2f}%",
            end_section=end_section,
        )

    total_prog = status["progress"].mean()
    table.add_row(
        "Total",
        f"{len(status)}",
        ProgressBar(width=50, completed=total_prog),
        f"{total_prog:2.2f}%",
    )
    print(table)


def status(h2f_files, long, sort):
    """
    Display status of h2f files.
    h2f_files (list): directories/files of h2f files to display the status of.
    long (bool): Displays statuses in long format if true. Otherwise, a summary is displayed.
    sort (string): How to sort the long-formatted statuses (either by 'progress' or 'filename')
    """
    if len(h2f_files) == 0:
        print("h2f_dir not found.")
        return

    file_list = make_filelist(h2f_files)
    status = h2f_2_status(file_list)

    if long:
        pretty_print_progress(status, sort)
    else:
        pretty_print_summary(status)