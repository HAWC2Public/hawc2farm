import time
import numpy as np
import pandas as pd
import itertools
from pydantic import BaseModel, Extra


def KeyError_message(param, instance):
    return f"Attribute '{param}' is required by '{instance.__class__.__name__}' class, but not found in kwargs"


def all_subclasses(cls):
    """
    Returns a list of all subclasses and subsubclasses, etc. through recursion.
    https://stackoverflow.com/a/3862957
    """
    return set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in all_subclasses(c)]
    )


class MyBaseModel(BaseModel):
    """
    Pydantic BaseModel which will ignore extra fields in initialization, but
    will allow adding them after and during initialization.
    """

    class Config:
        extra = "ignore"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__config__.extra = Extra.allow

    @classmethod
    def get_subclass(cls, name):
        for _cls in cls.all_subclasses():
            if _cls.__name__.lower() == name.lower():
                return _cls
        else:
            raise AttributeError(f"{name} not found.")

    @classmethod
    def all_subclasses(cls):
        return all_subclasses(cls)


def from_CVF(constants, variables, functionals):
    """
    Produces a DataFrame of contents given dictionaries of constants, variables
    and functionals.

    Args:
        constants (dict): content, value pairs for which the value remains
            constant for all generated htc files.
        variables (dict of list): content, list pairs for which all combinations
            of the listed values are generated in the htc files.
        functionals (dict of functions): content, function pairs for which the
            content is dependent on the constants and variables contents.

    Returns: contents (Dataframe): Dataframe of outputs.
    """

    attributes = (
        list(constants.keys()) + list(variables.keys()) + list(functionals.keys())
    )
    contents = []

    for var in itertools.product(*list(variables.values())):
        this_dict = dict(constants)
        var_dict = dict(zip(variables.keys(), var))
        this_dict.update(var_dict)

        for key, func in functionals.items():
            this_dict[key] = func(this_dict)

        contents.append(list(this_dict.values()))

    contents = pd.DataFrame(contents, columns=attributes)

    return contents


def load_mann_binary(filename, N=(32, 32)):
    """
    Loads a mann turbulence box in HAWC2 binary format.

    Args:
        filename (str): Filename of turbulence box
        N (tuple): Number of grid points (ny, nz) or (nx, ny, nz)

    Returns:
        turbulence_box (nd_array): turbulent box data as 3D array,
    """
    data = np.fromfile(filename, np.dtype("<f"), -1)
    if len(N) == 2:
        ny, nz = N
        nx = len(data) / (ny * nz)
        assert nx == int(
            nx
        ), f"Size of turbulence box ({len(data)}) does not match ny x nz ({ny*nx}), nx={nx}"
        nx = int(nx)
    else:
        nx, ny, nz = N
        assert (
            len(data) == nx * ny * nz
        ), "Size of turbulence box (%d) does not match nx x ny x nz (%d)" % (
            len(data),
            nx * ny * nz,
        )
    return data.reshape(nx, ny, nz)
