# HAWC2Farm
This repository contains the code for the aeroelastic windfarm simulator, HAWC2Farm. 

# Requirements
The code in this repository requires Python >=3.7.1, <3.11, numpy, scipy, pandas, mpi4py, pyyaml, pydantic, click, rich, [jDWM](https://gitlab.windenergy.dtu.dk/jyli/jdwm), [Pyhawc2](https://gitlab.windenergy.dtu.dk/sgho/pyhawc2)

and a HAWC2Farm compatible version of HAWC2.

# Installation
Clone this repository and pip install:
```
git clone https://gitlab.windenergy.dtu.dk/jaime-s-phd/hawc2farm.git
pip install hawc2farm
```
Configure hawc2farm using its command line interface by specifying the HAWC2 shared object path:
```
hawc2farm config --set-hawc2 path/to/HAWC2MB.so
```

# Usage
HAWC2Farm has a command line interface. A number of commands to run, validate input files, and check simulation statuses are available. You can check them by running the help command:
```
hawc2farm --help
```
